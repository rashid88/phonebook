<?php

use Illuminate\Database\Seeder;

class ContactsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$faker = \Faker\Factory::create();

        for($i=1; $i<=10; $i++){
        	$contact=\App\Contact::create([
	            'firstname' => $faker->firstName,
	            'lastname' => $faker->lastName,
	            'phone' => $faker->phoneNumber,
	            'phone_type_id' => rand(1,4),
	            'user_id' => 1,
	        ]);
       }
    }
}
