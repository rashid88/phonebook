<?php

use Illuminate\Database\Seeder;

class PhoneTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $phone_type=\App\Phone_type::create([
            'name'=>'Home',
        ]);

        $phone_type=\App\Phone_type::create([
            'name'=>'Work',
        ]);

        $phone_type=\App\Phone_type::create([
            'name'=>'Cellular',
        ]);

        $phone_type=\App\Phone_type::create([
            'name'=>'Other',
        ]);
    }
}
