# DEPLOYMENT INSTRUCTION #

1. Clone the repo
2. Open git, run composer install
3. copy .env.example and rename as .env
4. Create database name as "phonebook"
5. Set db credential in .env
	- DB_HOST=localhost
	- DB_DATABASE=phonebook
	- DB_USERNAME=“your database user”
	- DB_PASSWORD=“your database password”
6. Set APP_URL in .env
7. Open git, run:
	- php artisan key:generate
	- php artisan migrate --seed
	- php artisan serve

# LOGIN CREDENTIAL #

1. Email: admin@test.com
2. Password: secret