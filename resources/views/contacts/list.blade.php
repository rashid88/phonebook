<?php
/**
 * Created by PhpStorm.
 * User: Monty
 * Date: 21-07-2015
 * Time: 13:51
 */
?>
@extends('app')

@section('content')
    <!--
    =================================
    INNER SECTION
    =================================
    -->
    <section id="inner" class="inner-section section">
      

            <!-- SECTION HEADING -->
            <h2 class="section-heading text-center wow fadeIn" data-wow-duration="1s">Contacts</h2>
            <div class="row">
                <div class="col-md-6 col-md-offset-3 text-center">
                    <p class="wow fadeIn" data-wow-duration="1s">View your all contacs here.</p>
                </div>
            </div>

            <div class="inner-wrapper row">
                <div class="col-md-12">

                    @include('parts/flash')

                    <a href="{{ url('/contacts/add') }}" class="btn btn-primary" style="margin-bottom: 30px;">Add New contact</a>

                    <!-- Table Starts Here -->
                    <table id="contacts" class="table table-bordered table-responsive table-striped">
                        <thead>
                        <tr>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Phone</th>
                            <th>Phone Type</th>
                            <th>Date Created</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($contacts as $contact)
                            <tr>
                                <td>{{ $contact->firstname }}</td>
                                <td>{{ $contact->lastname }}</td>
                                <td>{{ $contact->phone }}</td>
                                <td>{{ $contact->phone_type->name }}</td>
                                <td>{{ date("Y-m-d",strtotime($contact->created_at)) }}</td>
                                <td>
                                    <a class="btn btn-primary" href="{{ url('contacts/edit', $contact->id) }}" data-toggle="tooltip"  title="Edit"><i alt="Edit" class="fa fa-pencil">Edit</i></a>
                                    {{-- <a href="{{ url('contacts/delete', $contact->id) }}" data-toggle="tooltip"  title="Delete"><i alt="Delete" class="fa fa-trash">Delete</i></a> --}}
                                    <form method="GET" action="{{ url('contacts/delete', $contact->id) }}" accept-charset="UTF-8" style="display: inline" onsubmit="return confirm('Are you sure you want to delete this contact?')">
                                        <input name="_method" type="hidden" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <button type="submit" title="Delete" class="btn btn-danger">Delete</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <!-- Table Ends Here -->

                </div>
            </div>

        
    </section>
@endsection

@section('inline_js')
    <script type="text/javascript">
        $(document).ready(function(){
            $('#contacts').DataTable();
        });
    </script>
@endsection