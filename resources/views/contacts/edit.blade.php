@extends('app')

@section('content')
    <!--
    =================================
    INNER SECTION
    =================================
    -->
    <section id="inner" class="inner-section section">
        <div class="container">

            <!-- SECTION HEADING -->
            <h2 class="section-heading text-center wow fadeIn" data-wow-duration="1s">Contacts</h2>
            <div class="row">
                <div class="col-md-6 col-md-offset-3 text-center">
                    <p class="wow fadeIn" data-wow-duration="1s">Update your contacts here.</p>
                </div>
            </div>

            <div class="inner-wrapper row">
                <div class="col-md-12">

                    @include('parts/flash')

                    <form name="frm" id="frm" action="{{ action('ContactsController@update', [$contact->id]) }}" method="post" class="col-md-6 col-md-offset-3">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <!-- firstname -->
                        <div class="form-group">
                            <label for="firstname">First Name</label>
                            <input type="text" name="firstname" class="form-control" maxlen="255" id="firstname" placeholder="Enter first name" value="{{ $contact->firstname }}" />
                        </div>

                        <!-- lastname -->
                        <div class="form-group">
                            <label for="lastname">Last Name</label>
                            <input type="text" name="lastname" class="form-control" maxlen="255" id="lastname" placeholder="Enter last name" value="{{ $contact->lastname }}" />
                        </div>

                        <!-- Phone -->
                        <div class="form-group">
                            <label for="phone">Phone Number</label>
                            <input type="text" name="phone" class="form-control" maxlen="255" id="phone" placeholder="Enter Phone" value="{{ $contact->phone }}" />
                        </div>

                        <!-- phone_type_id -->
                        <div class="form-group">
                            <label for="description">Phone Type </label>
                            <select class="form-control" name="phone_type_id">
                                <option value="1" {{ $contact->phone_type_id == '1' ? 'selected' : '' }}>Home</option>
                                <option value="2" {{ $contact->phone_type_id == '2' ? 'selected' : '' }}>Work</option>
                                <option value="3" {{ $contact->phone_type_id == '3' ? 'selected' : '' }}>Cellular</option>
                                <option value="4" {{ $contact->phone_type_id == '4' ? 'selected' : '' }}>Other</option>
                            </select>
                        </div>

                        <!-- Submit -->
                        <a class="btn btn-danger" href="{{ url('contacts') }}" data-toggle="tooltip"  title="Edit"><i alt="Edit" class="fa fa-pencil">Back</i></a>
                        <button type="submit" name="submit" id="submit" class="btn btn-primary">Update Contact</button>
                    </form>

                </div>
            </div>

        </div>
    </section>
@endsection