<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Phone_type extends Model
{
 	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'phone_types';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];

    public function contacts()
    {
        return $this->hasMany(Contact::class);
    }

}
